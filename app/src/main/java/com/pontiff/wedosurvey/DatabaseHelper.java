package com.pontiff.wedosurvey;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class DatabaseHelper extends SQLiteOpenHelper {

    static String name = "survey";
     static int version = 1;


    String createTableUserSql = "CREATE TABLE if not exists `User` (\n" +
            "\t`id`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
            "\t`email`\tTEXT NOT NULL,\n" +
            "\t`password`\tTEXT NOT NULL\n" +
            ")";


    String findUserSql = ("select select email , password from User");

    public DatabaseHelper(Context context) {
        super(context, name, null, version);

        getWritableDatabase().execSQL(createTableUserSql);
        getReadableDatabase().execSQL(findUserSql);



    }



    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

}
