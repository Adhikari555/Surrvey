package com.pontiff.wedosurvey;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.androidquery.AQuery;

import java.net.URL;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Header;

public class MainActivity extends AppCompatActivity {

    private static final String BASE_URL = "http://staging.cloudyfox.com/";
    private static final String LOGIN_URL = "http://staging.cloudyfox.com/wedosurvey/public/api/v1/";

    EditText username, password;
    Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



    username = (EditText) findViewById(R.id.username);
    password = (EditText) findViewById(R.id.password);
    login = (Button) findViewById(R.id.ulogin);

    login.setOnClickListener(new View.OnClickListener() {
        String usernameValue = username.getText().toString();
        String passwordValue = password.getText().toString();

       Retrofit retrofit = new Retrofit.Builder()
               .baseUrl(LOGIN_URL)
               .addConverterFactory(GsonConverterFactory.create())
               .build();

        SurveyApi surveyApi = retrofit.create(SurveyApi.class);

        HashMap<String, String> headerMap = new HashMap<String, String>();


        // esma error bhanxa ani garna khojeko chai tyo api baata login garauna khojeko ho
        
        headerMap.put("Content-type", "application/json");
        Call<ResponseBody> call = surveyApi.login(headerMap,usernameValue,passwordValue,"json");







      //  @Override
        public void onClick(View view) {
            Intent intent = new Intent(MainActivity.this, Survey.class);
            startActivity(intent);
        }
    });
    }
}
