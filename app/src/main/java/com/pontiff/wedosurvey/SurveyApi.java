package com.pontiff.wedosurvey;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SurveyApi{

     String BASE_URL ="http://staging.cloudyfox.com/wedosurvey/public/api/v1/";


    @Headers("Content-Type: application/json")

    @GET(".json")

    //Call<Feed> getData();


    @POST("{login}")

    Call<ResponseBody> login(

            @HeaderMap Map<String, String> headers,

            @Path("login") String login,  //login

            @Query("username") String username,       //?user=codingwithmitch

            @Query("password") String password //&passwd=Mitchtabian1234!

            //&api-type=json

    );




    }

